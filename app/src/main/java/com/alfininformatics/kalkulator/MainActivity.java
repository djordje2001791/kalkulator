package com.alfininformatics.kalkulator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pritisnutoDugme(View v) {
        EditText br1_polje = (EditText)findViewById(R.id.prviBroj_unos);
        EditText br2_polje = (EditText)findViewById(R.id.drugiBroj_unos);
        EditText rez_polje = (EditText)findViewById(R.id.rezultat_ispis);

        if(br1_polje.getText().toString().matches("") || br2_polje.getText().toString().matches("")) {
            Toast.makeText(this, "Niste unijeli broj! ", Toast.LENGTH_SHORT).show();
            return;
        }

        double br1, br2, rezultat;

        br1 = Double.parseDouble(br1_polje.getText().toString());
        br2 = Double.parseDouble(br2_polje.getText().toString());

        RadioGroup odabirOperacije = (RadioGroup)findViewById(R.id.operacije);
        int odabranaOperacija = odabirOperacije.getCheckedRadioButtonId();

        switch (odabranaOperacija) {
            case R.id.sabiranje_dugme:
                rezultat = br1 + br2;
                break;

            case R.id.oduzimanje_dugme:
                rezultat = br1 - br2;
                break;

            case R.id.mnozenje_dugme:
                rezultat = br1 * br2;
                break;

            case R.id.djeljenje_dugme:
                if (br2 != 0)
                    rezultat = br1 / br2;
                else {
                    Toast.makeText(this, "Nije moguće dijeliti sa nulom! ", Toast.LENGTH_SHORT).show();
                    return;
                }

                break;

            case R.id.stepenovanje_dugme:
                rezultat = Math.pow(br1, br2);
                break;

            default:
                rezultat = 0;
                Toast.makeText(this, "Greška! ", Toast.LENGTH_SHORT).show();

                break;
        }

        rez_polje.setText(Double.toString(rezultat));
    }
}
